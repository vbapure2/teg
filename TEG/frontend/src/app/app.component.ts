import { Component } from '@angular/core';
import { BackendService } from './backend.service';
import {pipe, Subscription, of} from 'rxjs';
import {map, catchError, finalize} from 'rxjs/operators';

export interface DataResponse {
  OrderHeader: OrderHeader;
  OrderLines: OrderLine[];
  PackageLines: PackageLine[];
}

interface OrderHeader {
  Id: number;
  OrderId: string;
  BasketId: string;
  DateTime: Date;
  CustomerId: number;
  OrganisationCustomerId: number;
  Seller: string;
  ProductId: string;
  ProductType: string;
}

interface OrderLine {
  Id: number;
  OrderLineId: string;
  InventoryType: string;
  PriceId: number;
  SheetId: number;
  OfferCode: string;
  Price: number;
  Gst: number;
  GstRate: string;
}

interface PackageLine {
  Id: number;
  PriceCategoryId: number;
  PriceCategoryCode: string;
  PriceTypeId: number;
  PriceTypeCode: string;
  PriceTypeName: string;
  Section: string;
  Row: string;
  Name: string;
  Barcode: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  students: any;
  data: DataResponse;
  displayedColumns: string[];
  loading: boolean;

  constructor(private backend: BackendService) { 
    // this.data = [];
    this.loading = true;
  }

  ngOnInit() {
    this.backend
        .getOrder('/api/order')
        .pipe(
          map(_ => this.data = _),
          finalize(() => this.loading = false)
        )
        .subscribe(_ => {
          console.log("Order Header: ", this.data.OrderHeader);
          console.log("Order Lines: ", this.data.OrderLines);
          console.log("Package Lines: ", this.data.PackageLines);
        });
  }
}
