import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, pipe, of } from 'rxjs';
import {map, catchError} from 'rxjs/operators';

import { DataResponse } from './app.component';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  params: HttpParams;

  constructor(private http: HttpClient) { 
    this.params = new HttpParams().set('Content-Type', 'application/json');
  }

  getOrder(api: string) : Observable<any> {  
    return this.http
               .get(api)
               .pipe(catchError(() => of([])));
  }
}
