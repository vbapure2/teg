﻿using backend.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace backend.Context
{
    public class OrderDbContext : DbContext
    {
        public OrderDbContext() : base("Test")
        {
            Database.SetInitializer(new DatabaseInitializer());
        }
        public DbSet<OrderHeader> OrderHeaders { get; set; }
        public DbSet<OrderLine> OrderLines { get; set; }
        public DbSet<PackageLine> PackageLines { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }

    public class DatabaseInitializer : DropCreateDatabaseIfModelChanges<OrderDbContext>
    {
        protected override void Seed(OrderDbContext context)
        {
            base.Seed(context);
        }
    }
}