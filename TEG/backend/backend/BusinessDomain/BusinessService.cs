﻿using backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace backend.BusinessDomain
{
    public interface IBusinessService
    {
        void UpdateS3ToPersistence();
        BusinessViewModel GetBusinessViewModel();
    }

    public class BusinessService : IBusinessService
    {
        public IS3Reader S3Reader { get; set; }
        public IOrderRepository OrderRepository { get; set; }

        public BusinessService(IS3Reader s3Reader, IOrderRepository orderRepository)
        {
            S3Reader = s3Reader;
            OrderRepository = orderRepository;
        }

        public void UpdateS3ToPersistence()
        {
            //var s3order = S3Reader.ReadS3();


            //OrderRepository.InsertOrderHeader(GetDbOrderHeader(s3order));
            //OrderRepository.InsertOrderLines(GetDbOrderLines(s3order));
            // OrderRepository.InsertPackageLines(GetDbPackageLines(s3order));

            // This is temporary arrangement, Since not able to connect to S3 for retreival
            // When S3 is able to connect, comment below lines and uncomment above lines
            OrderRepository.InsertOrderHeader(GetDbOrderHeader_Temp());
            OrderRepository.InsertOrderLines(GetDbOrderLines_Temp());
            OrderRepository.InsertPackageLines(GetDbPackageLines_Temp());
        }

        public BusinessViewModel GetBusinessViewModel()
        {
            var bvm = new BusinessViewModel()
            {
                OrderHeader = OrderRepository.GetAllOrderHeader(),
                OrderLines = OrderRepository.GetAllOrderLines(),
                PackageLines = OrderRepository.GetAllPackageLines()
            };

            return bvm;
        }

        private OrderHeader GetDbOrderHeader(Order s3order)
        {
            var DbOrderHeader = new OrderHeader();

            DbOrderHeader.BasketId = Guid.Parse(s3order.basketId);
            DbOrderHeader.CustomerId = s3order.customer._id;
            DbOrderHeader.DateTime = DateTime.Parse(s3order.dateTime);
            DbOrderHeader.OrderId = s3order.orderId;
            DbOrderHeader.OrganisationCustomerId = s3order.customer.organisationCustomerId;
            DbOrderHeader.ProductId = s3order.productId;
            DbOrderHeader.ProductType = s3order.productType;
            DbOrderHeader.Seller = s3order.seller;


            return DbOrderHeader;
        }

        private List<OrderLine> GetDbOrderLines(Order s3order)
        {
            var lines = new List<OrderLine>();

            foreach(var orderline in s3order.orderlineItems)
            {
                if(orderline.inventory == null)
                {
                    continue;
                }
                var DbOrderLine = new OrderLine();

                DbOrderLine.OrderLineId = orderline._id;
                DbOrderLine.InventoryType = orderline.inventory.type;
                DbOrderLine.PriceId = orderline.inventory.price._id;
                DbOrderLine.SheetId = orderline.inventory.price.sheetId;
                DbOrderLine.OfferCode = orderline.inventory.price.offerCode;
                DbOrderLine.Price = orderline.inventory.price.price;
                DbOrderLine.Gst = orderline.inventory.price.gst;
                DbOrderLine.GstRate = orderline.inventory.price.gstRate;

                lines.Add(DbOrderLine);

            }
            
            return lines;
        }

        private List<PackageLine> GetDbPackageLines(Order s3order)
        {
            var packagelines = new List<PackageLine>();

            foreach (var orderline in s3order.orderlineItems)
            {
                if(orderline.inventory == null)
                {
                    continue;
                }
                foreach (var pkgline in orderline.inventory.packages)
                {

                    
                    var DbPackageLine = new PackageLine();
                    DbPackageLine.Barcode = pkgline.barcode;
                    DbPackageLine.Name = pkgline.name;
                    DbPackageLine.PriceCategoryCode = pkgline.priceCategoryCode;
                    DbPackageLine.PriceCategoryId = pkgline.priceCategoryId;
                    DbPackageLine.PriceTypeCode = pkgline.priceTypeCode;
                    DbPackageLine.PriceTypeId = pkgline.priceTypeId;
                    DbPackageLine.PriceTypeName = pkgline.priceTypeName;
                    DbPackageLine.Row = pkgline.row;

                    packagelines.Add(DbPackageLine);
                }

            }

            return packagelines;
        }

        private OrderHeader GetDbOrderHeader_Temp()
        {
            var oh = new OrderHeader()
            {
                BasketId = Guid.NewGuid(),
                CustomerId = 11,
                DateTime = DateTime.Now,
                OrderId = "orderId1",
                OrganisationCustomerId = 11,
                ProductId = "pid1",
                ProductType = "pt1",
                Seller = "seller1"
            };

            return oh;
        }

        private List<OrderLine> GetDbOrderLines_Temp()
        {
            var orderlines = new List<OrderLine>();
            orderlines.Add(new OrderLine()
            {
                OrderLineId = "olid1",
                InventoryType = "it1",
                PriceId = 11,
                SheetId = 21,
                OfferCode = "oc1",
                Price = 12,
                Gst = 232,
                GstRate = "gstr1"
            });

            orderlines.Add(new OrderLine()
            {
                OrderLineId = "olid2",
                InventoryType = "it2",
                PriceId = 12,
                SheetId = 22,
                OfferCode = "oc2",
                Price = 11,
                Gst = 231,
                GstRate = "gstr2"
            });

            orderlines.Add(new OrderLine()
            {
                OrderLineId = "olid3",
                InventoryType = "it3",
                PriceId = 13,
                SheetId = 23,
                OfferCode = "oc3",
                Price = 13,
                Gst = 233,
                GstRate = "gstr3"
            });

            return orderlines;
        }

        private List<PackageLine> GetDbPackageLines_Temp()
        {
            var packagelines = new List<PackageLine>();
            packagelines.Add(new PackageLine()
            {
                Barcode = "barcode1",
                Name = "name1",
                PriceCategoryCode = "pcc1",
                PriceCategoryId = 11,
                PriceTypeCode = "ptc1",
                PriceTypeId = 11,
                PriceTypeName = "ptn1",
                Section = "section1",
                Row = "row1"
            });

            packagelines.Add(new PackageLine()
            {
                Barcode = "barcode2",
                Name = "name2",
                PriceCategoryCode = "pcc2",
                PriceCategoryId = 12,
                PriceTypeCode = "ptc2",
                PriceTypeId = 12,
                PriceTypeName = "ptn2",
                Section = "section1",
                Row = "row2"
            });

            packagelines.Add(new PackageLine()
            {
                Barcode = "barcode3",
                Name = "name3",
                PriceCategoryCode = "pcc3",
                PriceCategoryId = 13,
                PriceTypeCode = "ptc3",
                PriceTypeId = 13,
                PriceTypeName = "ptn3",
                Section = "section1",
                Row = "row3"
            });

            return packagelines;
        }
    }
}