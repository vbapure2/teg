﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace backend.Models
{ 
    public class BusinessViewModel
    {
        public OrderHeader OrderHeader { get; set; }
        public List<OrderLine> OrderLines { get; set; }
        public List<PackageLine> PackageLines { get; set; }
    }

    public class OrderHeader
    {
        [Key]
        public int Id { get; set; }
        public string OrderId { get; set; }
        public Guid BasketId { get; set; }
        public DateTime DateTime { get; set; }
        public int CustomerId { get; set; }
        public int OrganisationCustomerId { get; set; }
        public string Seller { get; set; }
        public string ProductId { get; set; }
        public string ProductType { get; set; }
    }

    public class OrderLine
    {
        [Key]
        public int Id { get; set; }
        public string OrderLineId { get; set; }
        public string InventoryType { get; set; }
        public int PriceId { get; set; }
        public int SheetId { get; set; }
        public string OfferCode { get; set; }
        public int Price { get; set; }
        public int Gst { get; set; }
        public string GstRate { get; set; }
    }

    public class PackageLine
    {
        [Key]
        public int Id { get; set; }
        public int PriceCategoryId { get; set; }
        public string PriceCategoryCode { get; set; }
        public int PriceTypeId { get; set; }
        public string PriceTypeCode { get; set; }
        public string PriceTypeName { get; set; }
        public string Section { get; set; }
        public string Row { get; set; }
        public string Name { get; set; }
        public string Barcode { get; set; }
    }
}