﻿using backend.BusinessDomain;
using backend.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace backend.Controllers
{
    public class OrderController : ApiController
    {
        public IBusinessService BusinessService { get; set; }
        public OrderController()
        {
            BusinessService = new BusinessService(new S3Reader(), new OrderRepository(new OrderDbContext()));
        }
        // GET api/values
        public IHttpActionResult Get()
        {
            BusinessService.UpdateS3ToPersistence();

            return Ok(BusinessService.GetBusinessViewModel());
        }
    }
}
